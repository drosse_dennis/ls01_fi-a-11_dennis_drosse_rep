import java.util.Scanner;

public class Händler {

    public static void main(String[] args) {

        String artikel = liesString("was möchten sie bestellen");

    }

    public static String liesString(String text) {
        System.out.println("was möchten Sie bestellen?");
        Scanner myScanner = new Scanner(System.in);
        String artikel = myScanner.next();
        return artikel;

    }

    public static int anzahl(String text) {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Geben Sie die Anzahl ein:");
        int anzahl = myScanner.nextInt();
        return anzahl;
    }

    public static double preis(String text) {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Geben Sie den Nettopreis ein:");
        double preis = myScanner.nextDouble();
        return preis;
    }

    public static double mwst(String text) {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
        double mwst = myScanner.nextDouble();
        return mwst;
    }

    public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
        double preis = 1;
        double nettogesamtpreis = anzahl * preis;
        return nettogesamtpreis;
    }

    public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
        double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
        return bruttogesamtpreis;
    }

    public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,
            double mwst) {
        System.out.println("\tRechnung");
        System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
        System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

    }
}
