class Übung1 {

    public static void main(String[] args) {

        Übung1();
        Übung2();
        Übung3();
    }

    public static void Übung1() {

        int a = 5;
        int b = 6;

        if (a == b) {
            System.out.println("Das Ergebnis ist gleich ");
        } else {
            System.out.println("Das Ergebnis ist ungleich");
        }
    }

    public static void Übung2() {
        int c = 1;
        int d = 5;

        if (c < d) {
            System.out.println("Richtig");
        } else {
            System.out.println("Falsch");
        }

    }

    public static void Übung3() {

        int e = 10;
        int f = 4;

        if (e > f || e == f) {
            System.out.println("Das Ergebnis ist richtig");
        } else {
            System.out.println("Das Ergebnis ist falsch");
        }
    }
}