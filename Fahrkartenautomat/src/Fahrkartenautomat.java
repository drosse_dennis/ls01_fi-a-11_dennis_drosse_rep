﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
    	boolean limited = false;
    	
    	while (limited == false) {
    		
    		try {
	 			Thread.sleep(1000);
	 		} catch (InterruptedException e) {
	 			// TODO Auto-generated catch block
	 			e.printStackTrace();
	 		}
 
    		//fahrkartenbestellungErfassen 
    		float zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);

    	    // Geldeinwurf/FahrkartenBezahlen
    	    // -----------
    	    float eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);

    	    // Fahrscheinausgabe
    	    // -----------------
    	    fahrkartenAusgeben();

    	    // Rückgeldberechnung und -Ausgabe
    	    // -------------------------------
    	    //float rückgabebetrag = 
    	    		  
    		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
    		
			try {
	 			Thread.sleep(3000);
	 		} catch (InterruptedException e) {
	 			// TODO Auto-generated catch block
	 			e.printStackTrace();
	 		}	
			System.out.println("\nBereit für die nächste Fahrkartenbestellung\n");
    	}	
	}
    
    public static float fahrkartenbestellungErfassen (Scanner tastatur) {
         float zuZahlenderBetrag = 0;
         byte anzahlTickets = 0;
         boolean fehler  = true;
           
    	while (fehler == true) {	
    		System.out.print("Fahrkartenbestellvorgang:\n"
        			+ "=========================\n"
        			+ "\n"
        			+ "Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n"
        			+ "  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n"
        			+ "  Tageskarte Regeltarif AB [8,60 EUR] (2)\n"
        			+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n"
        			+ "\n"
        			+ "Ihre Wahl: "); 
    		
    		int fahrkarte = tastatur.nextInt();

        	switch (fahrkarte) {
       	    case 1 :
       	    	zuZahlenderBetrag = 2.90f;
       	    	fehler = false;
       		    break;
    	   
       	    case 2 :
       	    	zuZahlenderBetrag = 8.60f;
       	    	fehler = false;
       		    break;
       		    
       	    case 3 :
       	    	zuZahlenderBetrag = 23.50f;
       	    	fehler = false;
       	    	break;
    	   
    	    default:
    	    	System.out.print("\n>>>falsche Eingabe<<<\nBitte wählen Sie einen gültigen Tarif aus\n\n");
    	    	fehler = true;
    		    break;
       	    } 	
		}
    	
    	
    	fehler = true;
    	while (fehler == true) {	
    		System.out.print("Anzahl der Tickets: ");
            anzahlTickets = tastatur.nextByte();
            
            if (anzahlTickets <= 10 && anzahlTickets > 0 ) {
            	fehler = false;
            }
            else if (anzahlTickets > 10 || anzahlTickets <= 0) {
            	fehler = true;
            	System.out.println("Bitte wählen Sie mindestens 1 oder maximal 10 Ticket/s aus.\n");
            }		
		}
        
        zuZahlenderBetrag *= anzahlTickets;
        
        return zuZahlenderBetrag;
    }
    
    public static float fahrkartenBezahlen (float zuZahlen, Scanner tastatur) {
    	
    	float eingezahlterGesamtbetrag = 0.0f;
        while (eingezahlterGesamtbetrag < zuZahlen) {
        	
     	   System.out.printf("\nNoch zu zahlen: %.2f EURO\n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   float eingeworfeneMünze = tastatur.nextFloat();
            eingezahlterGesamtbetrag += eingeworfeneMünze;  
        }
        
        return eingezahlterGesamtbetrag; 
    }
    
    public static void fahrkartenAusgeben () {
    	
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben  (float eingezahlt, float zuZahlen){
    	
    	float rückgabebetrag = eingezahlt - zuZahlen;
        if(rückgabebetrag > 0.0f)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f€\n", rückgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen 
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            } 
            
            System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                    "vor Fahrtantritt entwerten zu lassen!\n"+
                    "Wir wünschen Ihnen eine gute Fahrt.\n\n");  
        }
    }
}