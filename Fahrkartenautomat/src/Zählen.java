import java.util.Scanner;

public class Zählen {
    public static void main(String[] args) {

        Scanner tastatur = new Scanner(System.in);

        System.out.println("Ab wann soll gezählt werden");
        int counter = tastatur.nextInt();

        for (int i = 1; i <= counter; i++) {
            System.out.println("i = " + i);
        }

        System.out.println("Ab wann soll gezählt werden");
        int counter2 = tastatur.nextInt();

        for (int i = counter2; i >= 1; i--) {
            System.out.println("i = " + i);
        }
    }
}