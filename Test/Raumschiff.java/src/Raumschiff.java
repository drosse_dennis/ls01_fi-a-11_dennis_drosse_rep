import java.util.ArrayList;

public class Raumschiff {
	// Attribute
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	static private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	public char[] get;

	// Konstruktoren
	public Raumschiff() {
	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzentNeu) {
		this.energieversorgungInProzent = energieversorgungInProzentNeu;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzentNeu) {
		this.schildeInProzent = schildeInProzentNeu;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzentNeu) {
		this.huelleInProzent = huelleInProzentNeu;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzentNeu) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzentNeu;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	// Methoden
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}

	public void photonentorpedoSchiessen(Raumschiff r) {
		if (photonentorpedoAnzahl < 1) {
			nachrichtAnAlle("-=*click*=-");
		} else {
			photonentorpedoAnzahl = -1;
			nachrichtAnAlle("*Torpedo abgeschossen*");
			treffer(r);

		}
	}

	public void phaserkanoneSchiessen(Raumschiff r) {
		if (energieversorgungInProzent <= 50) {
			nachrichtAnAlle("-=*click*=-");
		} else {
			energieversorgungInProzent = -50;
			nachrichtAnAlle("*Phaser abgeschossen*");
			treffer(r);

		}
	}

	private void treffer(Raumschiff r) {

	}

	public void nachrichtAnAlle(String message) {
		broadcastKommunikator.add(message);
	}

	static public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return broadcastKommunikator;
	}

	public void photonentorpedosLaden(int anzahlTorpedos) {

	}

	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int AndroidenAnzahl) {

	}

	public void zustandRaumschiff() {
		System.out.println("photonentorpedoAnzahl: " + photonentorpedoAnzahl);
		System.out.println("energieversorgungInProzent: " + energieversorgungInProzent);
		System.out.println("schildeInProzent: " + schildeInProzent + "%");
		System.out.println("huelleInProzent: " + huelleInProzent + "%");
		System.out.println("lebenserhaltungssystemeInProzent: " + lebenserhaltungssystemeInProzent + "%");
		System.out.println("androidenAnzahl: " + androidenAnzahl);
	}

	public void ladungsverzeichnisAusgeben() {
		int count = 0;
		for (Ladung item : ladungsverzeichnis) {
			count++;
			System.out.printf("Ladung %d\n", count);
			System.out.printf("Bezeichnung: %s\n", item.getBezeichnung());
			System.out.printf("Menge: %d\n", item.getMenge());
		}
	}

	public void ladungsverzeichnisAufraeumen() {

	}

}
